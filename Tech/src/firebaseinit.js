import * as firebase from 'firebase/app'
//import 'firebase/firestore'
import firebaseconfig from './firebaseconfig'

const firebaseapp = firebase.initializeApp(firebaseconfig)
//const firestore = firebaseapp.firestore()
//const settings = {/* your settings... */ timestampsInSnapshots: true}
//firestore.settings(settings)
export default firebaseapp
