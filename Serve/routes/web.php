<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('pages.welcome');
});
Route::get('/about', function () {
    return view('pages.about');
});
Route::get('/support', function () {
    return view('pages.support');
});
Route::get('/help_center', function () {
    return view('pages.help_center');
});
Route::get('/contect_us', function () {
    return view('pages.contect_us');
});