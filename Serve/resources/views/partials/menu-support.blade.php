<nav class="red lighten-1">
    <div class="nav-wrapper">
        <a href="/" class="brand-logo">Tech</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="/about">About</a></li>
            <li><a href="/support">Support</a></li>
            <li><a href="/help_center">Help Center</a></li>
            <li><a href="/contect_us">Contect Us</a></li>
        </ul>
    </div>
</nav>