@extends('layouts.website')

@section('title', 'Home')

@section('content')
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15180.960701116543!2d102.60710292173509!3d17.967559553027804!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x188ecc7d44a06c2d!2zQW1hem9uIENhZmUg4Lqt4Lqw4LuA4Lqh4LqK4Lqt4LqZIOC6hOC6suC7gOC6n-C7iA!5e0!3m2!1sen!2sla!4v1532520394533" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="container">
        <div class="row">
            <div class="col m10 offset-m1 s12">
                <h2 class="center-align">Contact Form</h2>
                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input id="first_name" type="text" class="validate">
                                <label for="first_name">First Name</label>
                            </div>
                            <div class="input-field col m6 s12">
                                <input id="last_name" type="text" class="validate">
                                <label for="last_name">Last Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6 s12">
                                <i class="mdi-content-mail prefix"></i>
                                <input id="email" type="email" class="validate" required>
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6 s12">
                                <i class="mdi-maps-store-mall-directory prefix"></i>
                                <input id="company" type="text" class="validate">
                                <label for="company">Company</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="message" class="materialize-textarea"></textarea>
                                <label for="message">Message</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <label for="budget">Budget</label>
                                <br/>
                            </div>
                            <div class="input-field col s12">
                                <select class="browser-default" id="budget">
                                    <option value="" selected disabled>Choose your option</option>
                                    <option value="1">&lt; $4000</option>
                                    <option value="2">$4000 - $9000</option>
                                    <option value="3">&gt; $9000</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <label>How Did You Find Us?</label>
                                <br/>
                            </div>
                            <div class="input-field col m3 s6 center-align">
                                <input name="group1" type="radio" id="google" />
                                <label for="google">Google</label>
                            </div>
                            <div class="input-field col m3 s6 center-align">
                                <input name="group1" type="radio" id="customer" />
                                <label for="customer">Customer</label>
                            </div>
                            <div class="input-field col m3 s6 center-align">
                                <input name="group1" type="radio" id="store" />
                                <label for="store">Store</label>
                            </div>
                            <div class="input-field col m3 s6 center-align">
                                <input name="group1" type="radio" id="other" />
                                <label for="other">Other</label>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="input-field col s12">
                                <label>Communication Preferences</label>
                                <br/>
                            </div>
                            <div class="input-field col m4 s12 center-align">
                                <input name="group2" type="checkbox" id="emailComm" />
                                <label for="emailComm">Email</label>
                            </div>
                            <div class="input-field col m4 s12 center-align">
                                <input name="group2" type="checkbox" id="callComm" />
                                <label for="callComm">Call Me</label>
                            </div>
                            <div class="input-field col m4 s12 center-align">
                                <input name="group2" type="checkbox" id="newsletter" />
                                <label for="newsletter">Newsletter</label>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col m12">
                                <p class="right-align"><button class="btn btn-large waves-effect waves-light" type="button" name="action">Send Message</button></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
