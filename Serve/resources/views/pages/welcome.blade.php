@extends('layouts.main')

@section('title', 'Home')

@section('content')
    <p>This is my body content.</p>
@endsection

@section('slider')

<ul class="slides">
    <li>
    <img src="http://lorempixel.com/580/150/nature/1"> <!-- random image -->
    <div class="caption center-align">
        <h3>This is our big Tagline!</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
    </div>
    </li>
    <li>
    <img src="http://lorempixel.com/580/150/nature/2"> <!-- random image -->
    <div class="caption left-align">
        <h3>Left Aligned Caption</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
    </div>
    </li>
    <li>
    <img src="http://lorempixel.com/580/150/nature/3"> <!-- random image -->
    <div class="caption right-align">
        <h3>Right Aligned Caption</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
    </div>
    </li>
    <li>
    <img src="http://lorempixel.com/580/150/nature/4"> <!-- random image -->
    <div class="caption center-align">
        <h3>This is our big Tagline!</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
    </div>
    </li>
</ul>
@endsection

@section('css')
    <style>
        .slider .indicators .indicator-item {
            background-color: #666666;
            border: 3px solid #ffffff;
            -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }
        .slider .indicators .indicator-item.active {
            background-color: #ffffff;
        }
        .slider {
            width: 100%;
            margin: 0 auto;
        }
        .slider .indicators {
            bottom: 60px;
            z-index: 100;
            /* text-align: left; */
        }
    </style>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('.slider').slider({height:300});
        });
    </script>
@endsection