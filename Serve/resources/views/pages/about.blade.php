@extends('layouts.website')

@section('title', 'Home')

@section('content')
    <p>This is my body content.</p>
    <div id="app">
        <app></app>
    </div>
@endsection

@section('css')
<link rel="stylesheet" href="/css/app.css">
@endsection

@section('js')
<script src="/js/app.js"></script>
@endsection