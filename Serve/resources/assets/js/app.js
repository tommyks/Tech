import Vue from 'vue'
import App from './components/App.vue'
import Vuetify from 'vuetify'
//import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './firebaseinit'


Vue.use(Vuetify)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
